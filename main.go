package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Iniciando Servidor...")

	router := mux.NewRouter()

	//GET, POST, PUT, DELETE, etc
	router.HandleFunc("/", handleGeneral)
	router.HandleFunc("/{id}", handleOne)

	server := http.Server{
		Addr:              ":8080",
		Handler:           router,
		ReadTimeout:       30 * time.Second,
		ReadHeaderTimeout: 30 * time.Second,
		WriteTimeout:      30 * time.Second,
		IdleTimeout:       30 * time.Second,
	}

	server.ListenAndServe()
}
