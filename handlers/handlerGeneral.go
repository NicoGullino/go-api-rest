package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

const (
	GET    string = "GET"
	POST   string = "POST"
	PUT    string = "PUT"
	DELETE string = "DELETE "
)

type InterfaceGeneral interface{
	
}

type HandlerGeneral struct {
	Id     string
	Nombre string
	Edad   int
}
//Attach
func (h HandlerGeneral) handleGeneral(writer http.ResponseWriter, request *http.Request) (string, error, int) {
	if request.Method == "GET" {
		json.NewEncoder(writer).Encode("GET")
	}
	if request.Method == "POST" {
		json.NewEncoder(writer).Encode("POST")
	}
	if request.Method == "PUT" {
		json.NewEncoder(writer).Encode("PUT")
	}
	if request.Method == "DELETE" {
		json.NewEncoder(writer).Encode("DELETE")
	}
	return "Hola mundo", nil, 1
}

func (h HandlerGeneral) handleOne(writer http.ResponseWriter, request *http.Request) {
	if request.Method == "GET" {
		id, encontrado := mux.Vars(request)["id"]
		if encontrado {
			cadena := "Se recibió el id = " + id
			writer.WriteHeader(http.StatusOK)
			json.NewEncoder(writer).Encode(cadena)
			return
		}

		writer.WriteHeader(http.StatusNotFound)
		json.NewEncoder(writer).Encode(nil)
	}
}
